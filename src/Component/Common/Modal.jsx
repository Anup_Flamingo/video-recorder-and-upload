import React, { Children } from 'react';
import Modal from 'react-modal';

const customStyles = {
    content: {
      margin:"0 20px",
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-55%, -50%)',
      maxWidth: "100%"
    },
  };

Modal.setAppElement('#reviewRoot');

  
function ModalComp(props) {

    const {children, isOpen, closeModal} = props;
  return (
    <Modal
        isOpen={isOpen}
        // onAfterOpen={afterOpenModal}
        // onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
    >
        {children}
    </Modal>
  )
}

export default ModalComp
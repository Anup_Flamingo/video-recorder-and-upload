import React, { useCallback, useRef, useState } from "react";
import Webcam from "react-webcam";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowsRotate,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import ModalComp from "./Common/Modal";
import UploadMedia from "../Services/UploadMedia"
import { RotatingLines } from "react-loader-spinner";
import { useEffect } from "react";

const videoConstraints = {
  facingMode: "user",
};

const initialVideosValue = {
  start: false,
  stop: false,
};

export default function ModalWeb(props) {
  const {
    openModal,
    setOpenModal,
    timer,
    uploadedFiles,
    setUploadedFiles,
    isLoading,
    setIsLoading,
    formValues,
    setFormvalues,
    reviewTextRef
  } = props;
  const [videoRecordingInfo, setVideoRecordingInfo] = useState(initialVideosValue);
  const [recordedChunks, setRecordedChunks] = useState([]);
  const [userCamera, setUserCamera] = useState(videoConstraints);
  const [screenShot, setScreenShot] = useState(null);
  const [loaderWidht, setLoaderWidth] = useState('16px')

  const webcamRef = useRef(null);
  const mediaRecordRef = useRef(null);
  const saveRecordingRef = useRef(null);
  const timerRef = useRef(null);

  useEffect(()=>{
    if(screenShot?.length >0){
      const fontSize = window.getComputedStyle(saveRecordingRef.current, null).getPropertyValue("font-size");
      setLoaderWidth(fontSize || '16px')
    }
  },[screenShot])

  const handleCloseModal = () => {
      if (timer) {
        handleCloseTimer();
      }
      setVideoRecordingInfo({ ...initialVideosValue });
      setOpenModal(false);
      setRecordedChunks([]);
      setScreenShot(null);
      reviewTextRef.current.style.zIndex = "auto"
      timer.current = null;
  };

  const handleStartRecording = useCallback(() => {
    try {
      setVideoRecordingInfo({ ...videoRecordingInfo, start: true });
      mediaRecordRef.current = new MediaRecorder(webcamRef.current.stream, {
        mimeType: "video/webm",
      });

      mediaRecordRef.current.addEventListener(
        "dataavailable",
        handleDataAvailable
      );

      mediaRecordRef.current.start();
      handleStartTimer();
    } catch (err) {}
  }, [webcamRef, mediaRecordRef, videoRecordingInfo]);

  const handleDataAvailable = useCallback(
    ({ data }) => {
      try {
        if (data.size > 0) {
          setRecordedChunks((prev) => prev.concat(data));
        }
      } catch (err) {}
    },
    [setRecordedChunks]
  );

  const handleStopRecording = useCallback(() => {
    try {
      const imageSrc = webcamRef.current.getScreenshot();
      setScreenShot(imageSrc);
      mediaRecordRef.current.stop();
      setVideoRecordingInfo({ ...videoRecordingInfo, stop: true });

      handleCloseTimer();
    } catch (err) {}
  }, [mediaRecordRef, webcamRef, setVideoRecordingInfo]);

  const handleSaveRecording = useCallback(() => {
    try {
      if (recordedChunks.length > 0) {
        const blob = new Blob(recordedChunks, {
          type: "video/webm",
        });
        console.log(blob);
        setIsLoading(true);

        UploadMedia({
          mediaType: "video",
          mediaFile: blob,
        })
          .then((res) => {
            if (res.status === 200) {
              // const url = URL.createObjectURL(blob);
              // console.log("Recorded");
              // console.log(url);
              // const a = document.createElement("a");
              // document.body.appendChild(a);
              // a.style = "display: none";
              // a.href = url;
              // a.download = "recordedVideo.webm";
              // a.click();
              // window.URL.revokeObjectURL(url);
              setUploadedFiles([
                ...uploadedFiles,
                { fileType: "imageWeb", url: screenShot },
              ]);
              setFormvalues({...formValues, "Assets": [...formValues.Assets, res?.data]})

              setIsLoading(false);
              
              handleCloseModal();
              setRecordedChunks([]);
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    } catch (err) {}
  }, [recordedChunks]);

  const checkMobileView = () => {
    try {
      let check = false;
      (function (a) {
        if (
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
            a
          ) ||
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
            a.substr(0, 4)
          )
        )
          check = true;
      })(navigator.userAgent || navigator.vendor || window.opera);
      return check;
    } catch (err) {}
  };

  const handleCameraFlip = () => {
    try {
      if (userCamera.facingMode === "user") {
        setUserCamera({
          ...userCamera,
          facingMode: "environment",
        });
      } else {
        setUserCamera({
          ...userCamera,
          facingMode: "user",
        });
      }
    } catch (err) {}
  };

  const handleStartTimer = () => {
    try {
      const startTimer = new Date().getTime();
      console.log(timer);
      timer.current = setInterval(() => {
        const currentTime = new Date().getTime();
        console.log("startTimer");
        console.log(startTimer);
        const diff = Math.floor((currentTime - startTimer) / 1000);
        // console.log(timerRef.current);
        if (timerRef.current) {
          const quotent = Math.floor(diff / 60);
          const remainder = diff % 60;
          const minutes = quotent > 9 ? quotent : `0${quotent}`;
          const seconds = remainder > 9 ? remainder : `0${remainder}`;
          timerRef.current.innerText = `${minutes}:${seconds}`;
          console.log(quotent, remainder, minutes, seconds);
        }
      }, 1000);
    } catch (err) {}
  };

  const handleCloseTimer = () => {
    clearInterval(timer.current);
  };

  return (
    <ModalComp isOpen={openModal}>
      <div className="bringin-review-text-right bringin-review-mb-2">
      <button className="bringin-review-bg-transparent bringin-review-border-none" disabled={isLoading?true:false} onClick={handleCloseModal}>
        <FontAwesomeIcon
          className={`${
            isLoading
              ? "bringin-review-opacity-70"
              : "bringin-review-cursor-pointer"
          }`}
          icon={faXmark}
        />
      </button>
      </div>
      <Webcam
        className="bringin-review-max-w-full"
        ref={webcamRef}
        videoConstraints={userCamera}
        audio={true}
        muted={true}
        // videoConstraints={mediaStreamConstraints}
      />

      <div className="bringin-review-mt-4 bringin-review-flex bringin-review-justify-between bringin-review-gap-4 bringin-review-flex-wrap">
        {checkMobileView() &&
        !videoRecordingInfo.start &&
        !videoRecordingInfo.stop ? (
          <button
            onClick={handleCameraFlip}
            className="bringinReviewRecordBtns bringin-review-mt-4 bringin-review-cursor-pointer bringin-review-border-none"
          >
            <FontAwesomeIcon icon={faArrowsRotate} />
          </button>
        ) : (
          ""
        )}

        {!videoRecordingInfo.start && !videoRecordingInfo.stop ? (
          <button
            className="bringinReviewRecordBtns bringin-review-mt-4 bringin-review-cursor-pointer bringin-review-border-none"
            onClick={handleStartRecording}
          >
            Start
          </button>
        ) : videoRecordingInfo.start && !videoRecordingInfo.stop ? (
          <>
            <button className="bringinReviewRecordBtns bringin-review-mt-4 bringin-review-cursor-pointer bringin-review-border-none">
              <span className="bringin-review-flex bringin-review-items-center bringin-review-gap-2">
                RECORDING{" "}
                <span className="bringin-review-block bringin-review-w-3 bringin-review-h-3 bringin-review-rounded bringin-review-bg-red-700"></span>{" "}
                <span ref={timerRef}>00:00</span>
              </span>
            </button>

            <button
              className="bringinReviewRecordBtns bringin-review-mt-4 bringin-review-cursor-pointer bringin-review-border-none"
              onClick={handleStopRecording}
            >
              STOP{" "}
              <span className="bringin-review-w-3 bringin-review-h-3 bringin-review-bg-white bringin-review-inline-block"></span>
            </button>
          </>
        ) : videoRecordingInfo.stop ? (
          <button
            ref={saveRecordingRef}
            onClick={handleSaveRecording}
            disabled={isLoading?true:false}
            className={`bringinReviewRecordBtns ${isLoading?"bringin-review-opacity-70":"bringin-review-cursor-pointer"}  bringin-review-mt-4  bringin-review-border-none bringin-review-flex bringin-review-items-center bringin-review-justify-center bringin-review-gap-[20px]`}
          >
            SAVE
            {isLoading ? (
              <RotatingLines
                strokeColor="grey"
                strokeWidth="5"
                animationDuration="0.75"
                width={loaderWidht}
                visible={true}
              />
            ) : (
              ""
            )}
          </button>
        ) : (
          ""
        )}
      </div>
    </ModalComp>
  );
}

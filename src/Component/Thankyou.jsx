import React from 'react'

function Thankyou({thankyoumessage}) {
  return (
    <div className='bringinReviewHeading bringin-review-text-center bringin-review-mt-4'>
        {thankyoumessage}
    </div>
  )
}

export default Thankyou
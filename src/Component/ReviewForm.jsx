import React, { useState, useRef, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faPlay, faPlus, faXmark } from "@fortawesome/free-solid-svg-icons";
import { Rating } from "react-simple-star-rating";
import ModalWeb from "./ModalWeb";
import UploadMedia from "../Services/UploadMedia";
import { RotatingLines } from "react-loader-spinner";
import DeleteMedia from "../Services/DeleteMedia";
import CreateForm from "../Services/CreateForm";
import { Editor } from "@tinymce/tinymce-react";
import DOMPurify from 'dompurify';
import Thankyou from "./Thankyou";

// Start:-- Testing
import "../Style/DummyStyle.css";
// End:-- Testing


const initialValues = {
    "Name" :"",
    "Email" :"",
    "Phone Number" :"",
    "Review Title" : "",
    "Review" :"",
    "Rating": 0,
    "Assets" :[]

}

function ReviewForm({ mainHeading, productId, productTitle, isLoading, setIsLoading, thankyoumessage }) {
  const [openModal, setOpenModal] = useState(false);
  const [formValues, setFormvalues] = useState(initialValues);
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [writeReview, setWriteReview] = useState("");
  const [submitForm, setSubmitForm] = useState(false);

  const timer = useRef();
  const reviewTextRef = useRef();
  const errMsgRef = useRef();
  const validationErrRef = useRef();

  useEffect(() => {
    // console.log(uploadedFiles[0]?.name);
  }, [uploadedFiles]);

  const handleUploadVideoImage = (event) => {
    const type = event?.target?.files[0]?.type;

    if (type?.includes("video") || type?.includes("image")) {
      setIsLoading(true);
      console.log(event.target.files[0]);
      const url = URL.createObjectURL(event.target.files[0]);
      const fileType = type?.includes("video") ? "video" : "image";

      UploadMedia({
        mediaType: fileType,
        mediaFile: event.target.files[0],
      })
        .then((res) => {
          if (res.status === 200) {
            setFormvalues({...formValues, "Assets": [...formValues.Assets, res?.data]})
            setUploadedFiles([
              ...uploadedFiles,
              {
                fileType,
                url,
              },
            ]);
            setIsLoading(false);
            errMsgRef.current.classList.add("bringin-review-hidden");

          }
        })
        .catch((err) => {});
    } else {
      errMsgRef.current.classList.remove("bringin-review-hidden");
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let isValid = true;
    const errors = []
    
    validationErrRef.current.innerText=""
    errMsgRef.current.classList.add("bringin-review-hidden");


    if(!formValues["Name"]){
      isValid = false;
      errors.push("Name")
    }

    if(!formValues["Email"] && !formValues["Phone Number"]){
      isValid = false;
      errors.push("Email or Phone Number")
    }

    if(!formValues["Review Title"]){
      isValid = false;
      errors.push("Review Title")
    }

    if(formValues["Rating"] === 0){
      isValid = false;
      errors.push("Rating")
    }

    if(!writeReview){
      isValid = false;
      errors.push("Review")
    }
    else if(writeReview.length <10){
      isValid = false;
      errors.push("Minimum 10 characters are mandatory for write review section")
    }
    else{
    }

    if(!isValid && errors.length >0){
      const errorLenArr = errors.length;
      
      if(errorLenArr === 1){
        validationErrRef.current.innerText = `${errors[0]} is Mandatory.`
      }
      else{
        const errorFields = errors.slice(0, errorLenArr-1).join(", ")  + " and " + errors[errorLenArr -1]
        validationErrRef.current.innerText = `${errorFields} are Mandatory.`
      }
      return;
    }
    formValues["product_id"] =productId
    if(productId){
      CreateForm(formValues)  
      .then(res=>{
        // console.log(res);
        if(res.status === 200){
          setSubmitForm(true)
        }
      })
      .catch(err=>{})
    }

    
  };

  const handleDeleteMedia = (index, assetId) => {
    setIsLoading(true);
    DeleteMedia({assetId})
      .then((res) => {
        if (res.data?.error?.length === 0) {
          // Start:--- Removing Uploaded Files
          const duplicateUploadedFilesArr = [...uploadedFiles];
          // const indexOfUplaodedFile = duplicateUploadedFilesArr.indexOf(index);
          const newUploadedArr1 = duplicateUploadedFilesArr.slice(0, index);
          const newUploadedArr2 = duplicateUploadedFilesArr.slice(index + 1);
          setUploadedFiles([...newUploadedArr1, ...newUploadedArr2]);
          // End:--- Removing Uploaded Files

          // Start:--- Removing assetId
          // const duplicateAssetArr = [...asstesAttached];
          const duplicateAssetArr = [...formValues.Assets];

          const indexOfElement = duplicateAssetArr.indexOf(assetId);
          const newAssetArr1 = duplicateAssetArr.slice(0, indexOfElement);
          const newAssetArr2 = duplicateAssetArr.slice(indexOfElement + 1);
          setFormvalues({...formValues, "Assets": [...newAssetArr1, ...newAssetArr2]})
          // End:--- Removing assetId

          errMsgRef.current.classList.add("bringin-review-hidden");


        } else {
        }
        setIsLoading(false)
      })
      .catch((err) => {});
  };

  const handleEditorChange = (rawHTML, editor)=> {
    // const sanitizedBlog=DOMPurify.sanitize(rawHTML)
    // console.log(sanitizedBlog);
    // console.log("Raw");
    // console.log(rawHTML);
    const content = editor.getContent({format: 'text'})
    setWriteReview(content)
    setFormvalues({...formValues, "Review": rawHTML})
  }


  return (
    <>
      <div
        id="bringinReviewForm"
        className="bringin-review-border bringin-review-border-solid bringin-review-rounded-lg bringin-review-p-4 bringin-review-w-full sm:bringin-review-w-[640px] bringin-review-m-auto"
      >
        {
          submitForm?
          <Thankyou thankyoumessage={thankyoumessage}/>
          :
        <>
        <h1 className="bringinReviewHeading bringin-review-text-center">
          {mainHeading}
        </h1>
        <div className="bringin-review-flex bringin-review-flex-col bringin-review-gap-2 bringin-review-w-full">
          {/* Start:--Name */}
          <div>
            <label className="bringinReviewFormLabel" htmlFor="reviewName">
              Name <span className="bringin-review-text-red-600">*</span>
            </label>{" "}
            <br />
            <input
              className="bringinReviewTextBoxes bringin-review-px-2 bringin-review-py-3 bringin-review-w-full"
              type="text"
              placeholder="Full Name" 
              name="Name"
              id="reviewName"
              value={formValues["Name"]}
              onChange={(e)=> setFormvalues({...formValues, [e.target.name]: e.target.value})}
              // required
            />
          </div>
          {/* End:-- Name */}

          {/* Start:-- Email */}
          <div>
            <label className="bringinReviewFormLabel" htmlFor="reviewEmail">
              Email
            </label>{" "}
            <br />
            <input
              className="bringinReviewTextBoxes bringin-review-px-2 bringin-review-py-3 bringin-review-w-full"
              type="email"
              placeholder="Email"
              name="Email"
              id="reviewEmail"
              value={formValues["Email"]}
              onChange={(e)=> setFormvalues({...formValues, [e.target.name]: e.target.value})}
            />
          </div>
          {/* End:-- Email */}

          {/* Start:-- OR */}
          <div>
            <label className="bringinCustomErrorText bringinReviewFormLabel">OR</label>
          </div>
          {/* End:-- OR */}

          {/* Start:-- Phone Number */}
          <div>
            <label className="bringinReviewFormLabel" htmlFor="reviewPhone">
              Phone Number
            </label>{" "}
            <br />
            <input
              className="bringinReviewTextBoxes bringin-review-px-2 bringin-review-py-3 bringin-review-w-full"
              type="number"
              placeholder="Phone Number"
              name="Phone Number"
              id="reviewPhone"
              value={formValues["Phone Number"]}
              onChange={(e)=>{
                const value = e.target.value.toString()
                if(value.length <=10){
                  setFormvalues({...formValues, [e.target.name]: e.target.value})}
                }
              } 
            />
          </div>
          {/* End:-- Phone Number */}

          {/* Start:-- Review Title */}
          <div>
            <label className="bringinReviewFormLabel" htmlFor="reviewTitle">
              Review Title <span className="bringin-review-text-red-600">*</span>
            </label>{" "}
            <br />
            <input
              className="bringinReviewTextBoxes bringin-review-px-2 bringin-review-py-3 bringin-review-w-full"
              type="text"
              placeholder="Review Title"
              name="Review Title"
              id="reviewTitle"
              value={formValues["Review Title"]}
              onChange={(e)=> setFormvalues({...formValues, [e.target.name]: e.target.value})}
            />
          </div>
          {/* End:-- Review Title */}

          {/* Start:-- Rating */}
          <div>
            <label className="bringinReviewFormLabel" htmlFor="reviewTitle">
              Rating <span className="bringin-review-text-red-600">*</span>
            </label>{" "}
            <br />
            <Rating
              allowFraction
              onClick={(rate) => setFormvalues({...formValues, "Rating": rate})}
              fillColorArray={[
                "#f14f45",
                "#f16c45",
                "#f18845",
                "#f1b345",
                "#f1d045",
              ]}
              transition
            />
          </div>
          {/* End:-- Rating */}

          {/* Start:-- Review Text */}
          <div ref={reviewTextRef}>
            <label className="bringinReviewFormLabel" htmlFor="">
              Write Review for {productTitle}<span className="bringin-review-text-red-600">*</span>
            </label>{" "}
            <br />
            <br />
            <Editor
              apiKey={process.env.REACT_APP_TINYMCE_API_KEY}
              value={formValues["Review"]}
              // initialValue={sampleContent} 
              // onChange={(e)=>console.log(e.target.value)}
              onEditorChange={handleEditorChange}
            />
          </div>
          {/* End:-- Review Text */}

          {/* Start:-- Upload Section */}
          <div>
            <label className="bringinReviewFormLabel" htmlFor="reviewText">
              Upload Image or Video
            </label>{" "}
            <div className="bringin-review-mt-2">
              <div className="bringin-review-flex bringin-review-gap-3 bringin-review-items-center">
                <label
                  htmlFor="uploadVideoPhoto"
                  className={`${
                    isLoading
                      ? "bringin-review-opacity-20"
                      : "bringin-review-cursor-pointer"
                  } bringin-review-py-3 bringin-review-px-5 bringin-review-block bringin-review-w-fit bringin-review-border bringin-review-border-dotted bringin-review-border-black bringin-review-rounded-lg`}
                >
                  <input
                    className="bringinReviewTextBoxes bringin-review-hidden"
                    accept="image/png, image/gif, image/jpeg, video/mp4,video/x-m4v,video/*"
                    type="file"
                    name="uploadVideoPhoto"
                    id="uploadVideoPhoto"
                    disabled={isLoading ? true : false}
                    onChange={handleUploadVideoImage}
                  />
                  <span className="">
                    <FontAwesomeIcon className="text-2xl" icon={faPlus} />
                  </span>
                </label>
                <div>
                  {isLoading ? (
                    <RotatingLines
                      strokeColor="grey"
                      strokeWidth="5"
                      animationDuration="0.75"
                      width="30"
                      visible={true}
                    />
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div
                ref={errMsgRef}
                className="bringinCustomErrorText bringinReviewFormLabel  bringin-review-hidden bringin-review-mt-2"
              >
                File Uploaded Should be Image or Video
              </div>
            </div>
            <div className="bringin-review-mt-2 bringin-review-flex bringin-review-flex-row bringin-review-flex-wrap bringin-review-gap-3">
              {uploadedFiles?.map((file, idx) => {
                return (
                  <div
                    className="bringin-review-relative bringin-review-flex bringin-review-items-center"
                    key={idx}
                  >
                    {file.fileType === "image" ? (
                      <>
                        <img
                          className="bringin-review-w-[100px] bringin-review-h-[100px] bringin-review-object-cover bringin-review-border bringin-review-border-slate-600"
                          src={file.url}
                          alt="img"
                        />
                        <FontAwesomeIcon
                          className="bringin-review-absolute bringin-review-top-[-6px] bringin-review-right-[-10px] bringin-review-cursor-pointer bringin-review-px-[7px] bringin-review-py-[5px]  bringin-review-bg-slate-200 bringin-review-rounded-full"
                          icon={faXmark}
                          onClick={() =>
                            handleDeleteMedia(idx, formValues.Assets[idx])
                          }
                        />
                      </>
                    ) : file.fileType === "imageWeb" ? (
                      <>
                        <img
                          className="bringin-review-w-[100px] bringin-review-h-[76px] bringin-review-object-cover bringin-review-border bringin-review-border-slate-600"
                          src={file.url}
                          alt="img"
                        />
                        <FontAwesomeIcon
                          className="bringin-review-absolute bringin-review-top-[-6px] bringin-review-right-[-10px] bringin-review-cursor-pointer bringin-review-px-[7px] bringin-review-py-[5px]  bringin-review-bg-slate-200 bringin-review-rounded-full"
                          icon={faXmark}
                          onClick={() =>
                            handleDeleteMedia(idx, formValues.Assets[idx])

                          }
                        />
                        <FontAwesomeIcon
                          className="bringin-review-absolute bringin-review-top-[8px] bringin-review-left-0 bringin-review-right-0 bringin-review-bottom-0 bringin-review-m-auto bringin-review-px-[7px] bringin-review-py-[5px] bringin-review-bg-slate-200 bringin-review-rounded-full"
                          icon={faPlay}
                        />
                      </>
                    ) : (
                      <>
                        <video
                          name={""}
                          autoPlay={false}
                          playsInline
                          className="bringin-review-h-[100px] bringin-review-w-[100px] bringin-review-block bringin-review-max-w-full bringin-review-object-contain bringin-review-overflow-clip"
                          preload="meta"
                        >
                          <source src={file.url} type="video/webm" />
                          <source src={file.url} type="video/mp4" />
                        </video>
                        <FontAwesomeIcon
                          className="bringin-review-absolute bringin-review-top-[-6px] bringin-review-right-[-10px] bringin-review-cursor-pointer bringin-review-px-[7px] bringin-review-py-[5px]  bringin-review-bg-slate-200 bringin-review-rounded-full"
                          icon={faXmark}
                          onClick={() =>
                            handleDeleteMedia(idx, formValues.Assets[idx])
                          }
                        />
                        <FontAwesomeIcon
                          className="bringin-review-absolute bringin-review-top-[8px] bringin-review-left-0 bringin-review-right-0 bringin-review-bottom-0 bringin-review-m-auto bringin-review-px-[7px] bringin-review-py-[5px] bringin-review-bg-slate-200 bringin-review-rounded-full"
                          icon={faPlay}
                        />
                      </>
                    )}
                  </div>
                );
              })}
            </div>
            <button
              className={`${
                isLoading
                  ? "bringin-review-opacity-50"
                  : "bringin-review-cursor-pointer"
              } bringinReviewRecordBtns bringin-review-mt-4 bringin-review-border-none`}
              onClick={() => {
                reviewTextRef.current.style.zIndex = -1
                setOpenModal(true)
              }}
              disabled={isLoading ? true : false}
            >
              RECORD VIDEO
            </button>
          </div>
          {/* End:-- Upload Section */}
        </div>
        
        <div ref={validationErrRef} className="bringinCustomErrorText bringinReviewFormLabel bringin-review-mt-4"></div>
        <div className="bringin-review-text-center">
          <button
            id="bringinReviewSubmitBtn"
            className={`${
              isLoading
                ? "bringin-review-opacity-70"
                : "bringin-review-cursor-pointer"
            } bringin-review-mt-4 bringin-review-border-none`}
            type="submit"
            disabled={isLoading ? true : false}
            onClick={handleSubmit}
          >
            Submit
          </button>
        </div>
        </>
        } 

      </div>
      <ModalWeb
        openModal={openModal}
        setOpenModal={setOpenModal}
        timer={timer}
        uploadedFiles={uploadedFiles}
        setUploadedFiles={setUploadedFiles}
        isLoading={isLoading}
        setIsLoading={setIsLoading}
        formValues={formValues}
        setFormvalues={setFormvalues}
        reviewTextRef={reviewTextRef}
      />
    </>
  );
}

export default ReviewForm;

import { useEffect, useRef, useState } from "react";
import ModalComp from "./Component/Common/Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCross, faPlus, faXmark } from "@fortawesome/free-solid-svg-icons";
import ModalWeb from "./Component/ModalWeb";
import ReviewForm from "./Component/ReviewForm";
import WaitForElementToDisplay from "./Component/Common/MountUI";
import Thankyou from "./Component/Thankyou";

const videoConstraints = {
  facingMode: "user"
};

function App() {
  
  // Testing
  // const [mounted, setMounted] = useState(true);
  // const [productId, setProductId] = useState(10);
  // Testing

  // Production
  const [mounted, setMounted] = useState(false);
  const [productId, setProductId] = useState(null);
  // Production
  const [productTitle, setProductTitle] = useState(null);
  const [mainHeading, setMainHeading] = useState("Review Form");
  const [thankyoumessage, setThankyoumessge] = useState("Thank You for your review!!!!")
  const [isLoading, setIsLoading] = useState(false);
   
  const timer = useRef();

  useEffect(()=>{
    WaitForElementToDisplay(
      ".maReviewForm",
      function () {
        setMounted(true);
      },
      100,
      5000
    );
  },[])

  useEffect(()=>{
    if (mounted) {
      // Production
      setMainHeading(document.querySelector(".maReviewForm")?.dataset?.mainheading);
      setThankyoumessge(document.querySelector(".maReviewForm")?.dataset?.thankyoumessage);
      setProductTitle(document.querySelector(".maReviewForm")?.dataset?.producttitle)
      setProductId(document.querySelector(".maReviewForm")?.dataset?.productId)
      // Production
    }
  },[mounted])

  return (
    <div className="bringin-review-mx-4 bringin-review-my-2 ">
      {
       mounted && <ReviewForm mainHeading={mainHeading} productId={productId} productTitle={productTitle} isLoading={isLoading} setIsLoading={setIsLoading} thankyoumessage={thankyoumessage}/>    
      }
    </div>
  );
}

export default App;

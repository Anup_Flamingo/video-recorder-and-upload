import axios from 'axios';
import { LIVE_URL } from '../config';

function UploadMedia(propObj) {
    const {mediaType, mediaFile} = propObj;

    const form  = new FormData();

    // Testing
    form.append('hostname', "teststorebringin.myshopify.com");
    // Testing
    // PROD
    // form.append('hostname', window.location.hostname);
    // PROD
    form.append('media_type', mediaType);
    form.append('media_file', mediaFile);

    return axios.post(`${LIVE_URL}/api/media`, form, {
        "headers": {
            "Content-Type": "multipart/formdata"
        }
    })
}
export default UploadMedia
import axios from "axios";
import { LIVE_URL } from "../config";

export default function CreateForm(postObj) {
    const payload = {   
        // Testing
        "hostname": "teststorebringin.myshopify.com",
        // Testing
        // Production
        // "hostname": window.location.hostname,
        // Production
        "product_id":postObj["product_id"],
        "reviewer_name": postObj["Name"],
        "reviewer_phone": postObj["Phone Number"],
        "reviewer_email": postObj["Email"],
        "title": postObj["Review Title"],
        "body": postObj["Review"],
        "rating": postObj["Rating"],
        "media_attached": postObj["Assets"]
    }
    
    return axios.post(`${LIVE_URL}/api/review`, payload)
}

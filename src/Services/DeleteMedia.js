import axios from 'axios';
import { LIVE_URL } from '../config';

function DeleteMedia(propObj){
    const {assetId} = propObj;

    return axios.delete(`${LIVE_URL}/api/media/${assetId}`)
}


export default DeleteMedia
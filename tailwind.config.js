/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  prefix:"bringin-review-",
  theme: {
    extend: {
      colors:{
        "main-color":  "var(--mainColorTheme)",
      }
    },
  },
  plugins: [],
  corePlugins:{
    preflight: false
  }
}
